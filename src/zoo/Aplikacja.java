package zoo;

import zoo.interfejs.Dziennik;
import zoo.interfejs.Panel;
import zoo.interfejs.PanelZKwadratów;
import zoo.silnik.Wektor;
import zoo.silnik.organizmy.zwierzęta.*;
import zoo.silnik.Świat;
import zoo.silnik.ŚwiatZKwadratów;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Aplikacja extends JFrame {
    private Świat świat;
    private Panel panel;
    private GridBagConstraints ograniczenia;
    private Dziennik dziennik;

    Aplikacja() {
        super("Zoo");
        ograniczenia = new GridBagConstraints();
    }

    private void utwórzInterfejs() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);

        GridBagLayout układ = new GridBagLayout();
        ograniczenia.fill = GridBagConstraints.NONE;
        ograniczenia.gridy = 1;
        ograniczenia.gridx = 0;
        getContentPane().setLayout(układ);

        JButton przyciskNastępnejTury = new JButton("Następna tura");
        przyciskNastępnejTury.setFocusable(false);

        przyciskNastępnejTury.addActionListener((ActionEvent event) -> {
            następnaTura();
        });
        add(przyciskNastępnejTury, ograniczenia);

        JButton przyciskKońcaSymulacji = new JButton("Koniec symulacji");
        przyciskKońcaSymulacji.setFocusable(false);

        przyciskKońcaSymulacji.addActionListener((ActionEvent event) -> {
            setVisible(false);
            dispose();
        });

        ograniczenia.gridx = 1;
        add(przyciskKońcaSymulacji, ograniczenia);

        JButton przyciskZapisywaniaSymulacji = new JButton("Zapisz");
        przyciskZapisywaniaSymulacji.setFocusable(false);

        przyciskZapisywaniaSymulacji.addActionListener((ActionEvent event) -> {
            try {
                FileOutputStream plik = new FileOutputStream("save.dat");
                ObjectOutputStream strumień = new ObjectOutputStream(plik);
                strumień.writeObject(świat);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        ograniczenia.gridy = 2;
        ograniczenia.gridx = 0;
        add(przyciskZapisywaniaSymulacji, ograniczenia);

        JButton przyciskŁadowaniaSymulacji = new JButton("Ładuj");
        przyciskŁadowaniaSymulacji.setFocusable(false);

        przyciskŁadowaniaSymulacji.addActionListener((ActionEvent event) -> {
            try {
                FileInputStream plik = new FileInputStream("save.dat");
                ObjectInputStream strumień = new ObjectInputStream(plik);
                świat = (Świat) strumień.readObject();
                świat.ustawPanel(panel);
                świat.rysowanie();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }

        });

        ograniczenia.gridy = 2;
        ograniczenia.gridx = 1;
        add(przyciskŁadowaniaSymulacji, ograniczenia);

        dziennik = new Dziennik();

        ograniczenia.gridx = 10;
        ograniczenia.gridy = 0;
        add(new JScrollPane(dziennik), ograniczenia);

        Map<Integer, Long> czasyNaciśnięć = new HashMap<>();
        KeyboardFocusManager.getCurrentKeyboardFocusManager()
                .addKeyEventDispatcher(keyEvent -> {
                    int kod = keyEvent.getKeyCode();

                    long czasOdNaciśnięcia = Long.MAX_VALUE;
                    long teraz = System.currentTimeMillis();
                    if (czasyNaciśnięć.containsKey(kod)) {
                        czasOdNaciśnięcia = teraz - czasyNaciśnięć.get(kod);
                    }

                    if (czasOdNaciśnięcia >= 125) {
                        czasyNaciśnięć.put(kod, teraz);

                        switch (kod) {
                            case KeyEvent.VK_SPACE:
                                następnaTura();
                                return true;
                            case KeyEvent.VK_RIGHT:
                                przesuńCzłowieka(new Wektor(1, 0));
                                return true;
                            case KeyEvent.VK_LEFT:
                                przesuńCzłowieka(new Wektor(-1, 0));
                                return true;
                            case KeyEvent.VK_UP:
                                przesuńCzłowieka(new Wektor(0, -1));
                                return true;
                            case KeyEvent.VK_DOWN:
                                przesuńCzłowieka(new Wektor(0, 1));
                                return true;
                            case KeyEvent.VK_ENTER:
                                aktywujUmiejętność();
                                return true;
                        }
                    }
                    return false;
                });

        pack();
        setVisible(true);
        setLocationRelativeTo(null);
    }

    private void następnaTura() {
        dziennik.wyczyść();

        świat.wykonajTurę();
        świat.rysowanie();
    }

    private void przesuńCzłowieka(Wektor kierunek) {
        Człowiek człowiek = świat.pobierzCzłowieka();
        if (człowiek != null && człowiek.jestŻywy()) {
            człowiek.ustawKierunekRuchu(kierunek);
            następnaTura();
        }
    }

    private void aktywujUmiejętność() {
        Człowiek człowiek = świat.pobierzCzłowieka();
        if (człowiek != null && człowiek.jestŻywy()) {
            człowiek.aktywujUmiejętność();
            świat.rysowanie();
        }
    }

    private void rozpocznijGrę() {
        if (panel != null) {
            remove(panel);
        }

        świat = new ŚwiatZKwadratów(10, 10);
        świat.dodajObserwatora(obiekt ->
                dziennik.dodajWiadomość(obiekt)
        );

        świat.dodajOrganizm(new Owca(świat, new Wektor(5, 5)));
        świat.dodajOrganizm(new Owca(świat, new Wektor(1, 1)));
        świat.dodajOrganizm(new Wilk(świat, new Wektor(1, 5)));
        świat.dodajOrganizm(new Antylopa(świat, new Wektor(9, 9)));

        Człowiek człowiek = new Człowiek(świat, new Wektor(5, 0));
        świat.dodajOrganizm(człowiek);
        świat.ustawCzłowieka(człowiek);

        panel = new PanelZKwadratów(świat);

        ograniczenia.gridx = 0;
        ograniczenia.gridy = 0;
        ograniczenia.gridwidth = 2;

        add(panel, ograniczenia);
        świat.ustawPanel(panel);
        świat.rysowanie();
        pack();
    }

    static public void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            Aplikacja aplikacja = new Aplikacja();
            aplikacja.utwórzInterfejs();
            aplikacja.rozpocznijGrę();
        });
    }
}
