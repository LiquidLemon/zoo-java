package zoo;

import zoo.interfejs.Dziennik;
import zoo.interfejs.Panel;
import zoo.interfejs.PanelZKwadratów;
import zoo.silnik.FabrykaŚwiatów;
import zoo.silnik.Wektor;
import zoo.silnik.organizmy.zwierzęta.*;
import zoo.silnik.Świat;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Aplikacja extends JFrame {
    private Świat świat;
    private Panel panel;
    private GridBagConstraints ograniczenia;
    private Dziennik dziennik;

    Aplikacja() {
        super("Zoo");
        ograniczenia = new GridBagConstraints();
    }

    private void utwórzInterfejs() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);

        GridBagLayout układ = new GridBagLayout();
        ograniczenia.fill = GridBagConstraints.NONE;
        ograniczenia.gridy = 1;
        ograniczenia.gridx = 0;
        getContentPane().setLayout(układ);

        JButton przyciskNastępnejTury = new JButton("Następna tura");
        przyciskNastępnejTury.setFocusable(false);

        przyciskNastępnejTury.addActionListener((ActionEvent event) -> {
            następnaTura();
        });
        add(przyciskNastępnejTury, ograniczenia);

        JButton przyciskKońcaSymulacji = new JButton("Koniec symulacji");
        przyciskKońcaSymulacji.setFocusable(false);

        przyciskKońcaSymulacji.addActionListener((ActionEvent event) -> {
            setVisible(false);
            dispose();
        });

        ograniczenia.gridx = 1;
        add(przyciskKońcaSymulacji, ograniczenia);

        JButton przyciskZapisywaniaSymulacji = new JButton("Zapisz");
        przyciskZapisywaniaSymulacji.setFocusable(false);

        przyciskZapisywaniaSymulacji.addActionListener((ActionEvent event) -> {
            try {
                FileOutputStream plik = new FileOutputStream("save.dat");
                ObjectOutputStream strumień = new ObjectOutputStream(plik);
                strumień.writeObject(świat);
            } catch (IOException e) {
                System.out.println("Nie udało się zapisać symulacji");
            }
        });

        ograniczenia.gridy = 2;
        ograniczenia.gridx = 0;
        add(przyciskZapisywaniaSymulacji, ograniczenia);

        JButton przyciskŁadowaniaSymulacji = new JButton("Ładuj");
        przyciskŁadowaniaSymulacji.setFocusable(false);

        przyciskŁadowaniaSymulacji.addActionListener((ActionEvent event) -> {
            try {
                FileInputStream plik = new FileInputStream("save.dat");
                ObjectInputStream strumień = new ObjectInputStream(plik);
                świat = (Świat) strumień.readObject();
                utwórzPanel();
                świat.ustawPanel(panel);
                świat.rysowanie();
            } catch (IOException | ClassNotFoundException e) {
                System.out.println("Nie udało się załadować symulacji");
            }

        });

        ograniczenia.gridy = 2;
        ograniczenia.gridx = 1;
        add(przyciskŁadowaniaSymulacji, ograniczenia);

        dziennik = new Dziennik();

        ograniczenia.gridx = 10;
        ograniczenia.gridy = 0;

        JScrollPane pole = new JScrollPane(dziennik);
        pole.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        add(pole, ograniczenia);

        Map<Integer, Long> czasyNaciśnięć = new HashMap<>();
        KeyboardFocusManager.getCurrentKeyboardFocusManager()
                .addKeyEventDispatcher(keyEvent -> {
                    int kod = keyEvent.getKeyCode();

                    long czasOdNaciśnięcia = Long.MAX_VALUE;
                    long teraz = System.currentTimeMillis();
                    if (czasyNaciśnięć.containsKey(kod)) {
                        czasOdNaciśnięcia = teraz - czasyNaciśnięć.get(kod);
                    }

                    if (czasOdNaciśnięcia >= 125) {
                        czasyNaciśnięć.put(kod, teraz);

                        switch (kod) {
                            case KeyEvent.VK_SPACE:
                                następnaTura();
                                return true;
                            case KeyEvent.VK_RIGHT:
                                przesuńCzłowieka(new Wektor(1, 0));
                                return true;
                            case KeyEvent.VK_LEFT:
                                przesuńCzłowieka(new Wektor(-1, 0));
                                return true;
                            case KeyEvent.VK_UP:
                                przesuńCzłowieka(new Wektor(0, -1));
                                return true;
                            case KeyEvent.VK_DOWN:
                                przesuńCzłowieka(new Wektor(0, 1));
                                return true;
                            case KeyEvent.VK_ENTER:
                                aktywujUmiejętność();
                                return true;
                        }
                    }
                    return false;
                });

        pack();
        setVisible(true);
        setLocationRelativeTo(null);
    }

    private void następnaTura() {
        dziennik.wyczyść();

        świat.wykonajTurę();
        świat.rysowanie();
    }

    private void przesuńCzłowieka(Wektor kierunek) {
        Człowiek człowiek = świat.pobierzCzłowieka();
        if (człowiek != null && człowiek.jestŻywy()) {
            człowiek.ustawKierunekRuchu(kierunek);
            następnaTura();
        }
    }

    private void aktywujUmiejętność() {
        Człowiek człowiek = świat.pobierzCzłowieka();
        if (człowiek != null && człowiek.jestŻywy()) {
            człowiek.aktywujUmiejętność();
            świat.rysowanie();
        }
    }

    private void rozpocznijGrę(int szerokość, int wysokość) {
        świat = FabrykaŚwiatów.nowyŚwiatZKwadratów(szerokość, wysokość);
        świat.dodajObserwatora(obiekt ->
                dziennik.dodajWiadomość(obiekt)
        );

        utwórzPanel();
    }

    private void utwórzPanel() {
        if (panel != null) {
            remove(panel);
        }

        panel = new PanelZKwadratów(świat);

        ograniczenia.gridx = 0;
        ograniczenia.gridy = 0;
        ograniczenia.gridwidth = 2;

        add(panel, ograniczenia);
        świat.ustawPanel(panel);
        świat.rysowanie();
        pack();
    }

    static public void main(String[] args) {
        int szerokość = args.length > 0
                ? Integer.parseInt(args[0])
                : 20;
        int wysokość = args.length > 1
                ? Integer.parseInt(args[1])
                : 20;

        SwingUtilities.invokeLater(() -> {
            Aplikacja aplikacja = new Aplikacja();
            aplikacja.utwórzInterfejs();
            aplikacja.rozpocznijGrę(szerokość, wysokość);
        });
    }
}
