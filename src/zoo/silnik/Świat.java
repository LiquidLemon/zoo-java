package zoo.silnik;

import zoo.interfejs.Panel;
import zoo.narzędzia.GeneratorLiczbLosowych;
import zoo.narzędzia.Obserwowalny;
import zoo.silnik.organizmy.zwierzęta.Człowiek;
import zoo.silnik.wydarzenia.NowyOrganizm;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public abstract class Świat
        extends Obserwowalny<Object>
        implements Serializable
{
    protected List<Organizm> organizmy;
    protected List<Organizm> kolejkaDodawania;
    protected Organizm[][] plansza;
    protected int wysokość, szerokość;
    protected Człowiek człowiek;
    protected Panel panel;

    public Świat(int wysokość, int szerokość) {
        this.wysokość = wysokość;
        this.szerokość = szerokość;
        plansza = new Organizm[szerokość][wysokość];
        organizmy = new ArrayList<>();
        kolejkaDodawania = new ArrayList<>();
    }

    public abstract void rysowanie();

    public void wykonajTurę() {
        organizmy.addAll(kolejkaDodawania);
        kolejkaDodawania.clear();

        for (Organizm organizm : organizmy) {
           if (organizm.jestŻywy()) {
               organizm.akcja();
               organizm.postarz();
           }
        }

        organizmy = organizmy.stream()
                .filter(Organizm::jestŻywy)
                .collect(Collectors.toList());

        organizmy.sort((Organizm a, Organizm b) -> {
            if (a == b) return 0;
            if (a.pobierzInicjatywę() == b.pobierzInicjatywę()) {
                return a.pobierzWiek() > b.pobierzWiek() ? 1 : -1;
            }
            return a.pobierzInicjatywę() > b.pobierzInicjatywę() ? 1 : -1;
        });
    }

    public void dodajOrganizm(Organizm organizm) {
        plansza[organizm.pozycja.x][organizm.pozycja.y] = organizm;
        kolejkaDodawania.add(organizm);
        emituj(new NowyOrganizm(organizm));
        organizm.dodajObserwatora(this::emituj);
    }

    public void ustawCzłowieka(Człowiek człowiek) {
        this.człowiek = człowiek;
    }

    public Człowiek pobierzCzłowieka() {
        return człowiek;
    }

    public abstract Set<Wektor> pobierzSąsiadów(Wektor pozycja);

    public boolean poleIstnieje(Wektor pozycja) {
        return pozycja.x >= 0 && pozycja.x < szerokość &&
               pozycja.y >= 0 && pozycja.y < wysokość;
    }

    public Wektor pobierzLosowegoSąsiada(Wektor pozycja) {
        Set<Wektor> sąsiedzi = pobierzSąsiadów(pozycja);
        return GeneratorLiczbLosowych.losowyElement(sąsiedzi);
    }

    public Set<Wektor> pobierzPustychSąsiadów(Wektor pozycja) {
        return pobierzSąsiadów(pozycja).stream().filter(
                sąsiad -> plansza[sąsiad.x][sąsiad.y] == null
        ).collect(Collectors.toSet());
    }

    public Organizm pobierzOrganizm(Wektor pozycja) {
        return plansza[pozycja.x][pozycja.y];
    }

    public void przesuńOrganizm(Organizm organizm, Wektor nowaPozycja) {
        Organizm innyOrganizm = plansza[nowaPozycja.x][nowaPozycja.y];
        if (innyOrganizm == null ||
            (innyOrganizm != organizm && !innyOrganizm.kolizja(organizm)))
        {
                plansza[organizm.pozycja.x][organizm.pozycja.y] = null;
                plansza[nowaPozycja.x][nowaPozycja.y] = organizm;
                organizm.pozycja = nowaPozycja;
        }

    }

    public void usuńZPlanszy(Organizm organizm) {
        plansza[organizm.pozycja.x][organizm.pozycja.y] = null;
    }

    public int pobierzWysokość() {
        return wysokość;
    }

    public int pobierzSzerokość() {
        return szerokość;
    }

    public void ustawPanel(Panel panel) {
        this.panel = panel;
    }
}
