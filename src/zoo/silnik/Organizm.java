package zoo.silnik;

import zoo.narzędzia.GeneratorLiczbLosowych;
import zoo.narzędzia.Obserwowalny;
import zoo.silnik.wydarzenia.Walka;
import zoo.silnik.wydarzenia.Śmierć;

import java.io.Serializable;
import java.util.Set;

public abstract class Organizm
        extends Obserwowalny<Object>
        implements Serializable
{
    protected int inicjatywa;
    protected int siła;
    protected int wiek;
    protected Świat świat;
    protected Wektor pozycja;

    public Organizm(Świat świat, Wektor pozycja, int inicjatywa, int siła) {
       this.świat = świat;
       this.pozycja = pozycja;
       this.inicjatywa = inicjatywa;
       this.siła = siła;
       wiek = 0;
    }

    public String pobierzSymbol() {
        return "?";
    }

    public int pobierzInicjatywę() {
        return inicjatywa;
    }

    public int pobierzSiłę() {
        return siła;
    }

    public void ustawSiłę(int wartość) {
        siła = wartość;
    }

    public Wektor pobierzPozycję() {
        return pozycja;
    }

    public boolean jestŻywy() {
        return pozycja != null;
    }

    public int pobierzWiek() {
        return wiek;
    }

    public void postarz() {
        wiek++;
    }

    public void akcja() {

    }

    @Override
    public String toString() {
        return pobierzSymbol();
    }

    /**
     *
     * @param agresor atakujący organizm
     * @return        informuje czy pole jest nadal zajęte
     */
    public boolean kolizja(Organizm agresor) {
        if (agresor.getClass().equals(getClass())) {
            rozmnażaj(agresor);
        } else if (siła > agresor.siła) {
            emituj(new Walka(agresor, this));
            agresor.zabij();
        } else {
            emituj(new Walka(agresor, this));
            zabij();
            return false;
        }
        return true;
    }

    private void rozmnażaj(Organizm partner) {
        Set<Wektor> wolniSąsiedzi = świat.pobierzPustychSąsiadów(pozycja);
        wolniSąsiedzi.addAll(świat.pobierzPustychSąsiadów(partner.pozycja));

        if (!wolniSąsiedzi.isEmpty()) {
            Wektor pole = GeneratorLiczbLosowych.losowyElement(wolniSąsiedzi);
            try {
                Organizm potomek = getClass()
                        .getConstructor(Świat.class, Wektor.class)
                        .newInstance(świat, pole);

                świat.dodajOrganizm(potomek);
            } catch (ReflectiveOperationException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void zabij() {
        emituj(new Śmierć(this));
        świat.usuńZPlanszy(this);
        pozycja = null;
    }
}
