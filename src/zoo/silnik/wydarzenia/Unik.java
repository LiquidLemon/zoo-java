package zoo.silnik.wydarzenia;

import zoo.silnik.Organizm;

public class Unik {
    private final Organizm agresor;
    private final Organizm organizm;

    public Unik(Organizm organizm, Organizm agresor) {
        this.organizm = organizm;
        this.agresor = agresor;
    }

    @Override
    public String toString() {
        return "" + organizm + " " + organizm.pobierzPozycję() + " unika " +
                agresor + " " + agresor.pobierzPozycję();
    }
}
