package zoo.silnik.wydarzenia;

import zoo.silnik.Organizm;

public class NowyOrganizm {
    private final Organizm organizm;

    public NowyOrganizm(Organizm organizm) {
        this.organizm = organizm;
    }

    @Override
    public String toString() {
        return "Nowy organizm: "
                + organizm + " " + organizm.pobierzPozycję();
    }
}
