package zoo.silnik.wydarzenia;

import zoo.silnik.Organizm;

public class Walka {
    private final Organizm agresor;
    private final Organizm atakowany;

    public Walka(Organizm agresor, Organizm atakowany) {
        this.agresor = agresor;
        this.atakowany = atakowany;
    }

    @Override
    public String toString() {
        return "" + agresor + " " + agresor.pobierzPozycję() + " atakuje "
                + atakowany + " " + atakowany.pobierzPozycję();
    }
}
