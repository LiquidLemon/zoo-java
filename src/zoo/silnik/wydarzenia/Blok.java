package zoo.silnik.wydarzenia;

import zoo.silnik.Organizm;

public class Blok {
    private final Organizm organizm;
    private final Organizm agresor;

    public Blok(Organizm organizm, Organizm agresor) {
        this.organizm = organizm;
        this.agresor = agresor;
    }

    @Override
    public String toString() {
        return "" + organizm + " " + organizm.pobierzPozycję()
                + " blokuje atak " + agresor + " " + agresor.pobierzPozycję();
    }
}
