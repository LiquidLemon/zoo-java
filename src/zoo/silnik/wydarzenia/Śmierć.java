package zoo.silnik.wydarzenia;

import zoo.silnik.Organizm;

public class Śmierć {
    private final Organizm organizm;

    public Śmierć(Organizm organizm) {
        this.organizm = organizm;
    }

    @Override
    public String toString() {
        return "" + organizm + " " + organizm.pobierzPozycję() + " umiera";
    }
}
