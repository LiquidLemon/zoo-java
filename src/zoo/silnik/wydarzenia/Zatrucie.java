package zoo.silnik.wydarzenia;

import zoo.silnik.Organizm;

public class Zatrucie {
    private final Organizm organizm;
    private final Organizm ofiara;

    public Zatrucie(Organizm organizm, Organizm ofiara) {
        this.organizm = organizm;
        this.ofiara = ofiara;
    }

    @Override
    public String toString() {
        return "" + organizm + " " + organizm.pobierzPozycję() + " zatruwa "
                + ofiara + " " + ofiara.pobierzPozycję();
    }
}
