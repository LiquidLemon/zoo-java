package zoo.silnik.wydarzenia;

import zoo.silnik.Organizm;

public class Zjedzenie {
    private final Organizm organizm;
    private final Organizm agresor;

    public Zjedzenie(Organizm organizm, Organizm agresor) {
        this.organizm = organizm;
        this.agresor = agresor;
    }

    @Override
    public String toString() {
        return "" + agresor + " " + agresor.pobierzPozycję() + " zjada " +
                organizm + " " + organizm.pobierzPozycję();
    }
}
