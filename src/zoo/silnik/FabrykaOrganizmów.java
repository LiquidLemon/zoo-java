package zoo.silnik;

import zoo.silnik.organizmy.rośliny.*;
import zoo.silnik.organizmy.zwierzęta.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FabrykaOrganizmów {
    private static Map<String, Class> klasy;
    static {
        klasy = new HashMap<>();
        klasy.put("Człowiek", Człowiek.class);
        klasy.put("Wilk", Wilk.class);
        klasy.put("Antylopa", Antylopa.class);
        klasy.put("Lis", Lis.class);
        klasy.put("Owca", Owca.class);
        klasy.put("Żółw", Żółw.class);
        klasy.put("Guarana", Guarana.class);
        klasy.put("Mlecz", Mlecz.class);
        klasy.put("Trawa", Trawa.class);
        klasy.put("Wilcze jagody", WilczaJagoda.class);
        klasy.put("Barszcz Sosnowskiego", BarszczSosnowskiego.class);
    }

    public static Organizm nowyOrganizm(Class klasa,
                                        Świat świat, Wektor pozycja)
    {
        if (klasa.equals(Człowiek.class) && świat.pobierzCzłowieka() != null
                && świat.pobierzCzłowieka().jestŻywy()) {
            return null;
        }
        try {
            Organizm organizm = (Organizm) klasa
                    .getConstructor(Świat.class, Wektor.class)
                    .newInstance(świat, pozycja);
            świat.dodajOrganizm(organizm);
            if (organizm instanceof Człowiek) {
                świat.ustawCzłowieka((Człowiek) organizm);
            }
            return organizm;
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Organizm nowyOrganizm(String nazwa, Świat świat, Wektor pozycja) {
        return nowyOrganizm(klasy.get(nazwa), świat, pozycja);
    }

    public static Set<String> gatunki() {
        return klasy.keySet();
    }
}
