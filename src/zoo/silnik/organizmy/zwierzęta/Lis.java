package zoo.silnik.organizmy.zwierzęta;

import zoo.narzędzia.GeneratorLiczbLosowych;
import zoo.silnik.Organizm;
import zoo.silnik.Wektor;
import zoo.silnik.organizmy.Zwierzę;
import zoo.silnik.Świat;

import java.util.Set;
import java.util.stream.Collectors;

public class Lis extends Zwierzę {
    public Lis(Świat świat, Wektor pozycja) {
        super(świat, pozycja, 7, 3);
    }

    @Override
    public String pobierzSymbol() {
        return "\uD83D\uDC31"; // 🐱
    }

    @Override
    public void akcja() {
        Set<Wektor> dostępnePola = świat.pobierzSąsiadów(pozycja).stream()
                .filter((Wektor pozycja) -> {
                    Organizm organizm = świat.pobierzOrganizm(pozycja);
                    return organizm == null || organizm.pobierzSiłę() <= siła;
                }).collect(Collectors.toSet());

        if (!dostępnePola.isEmpty()) {
            Wektor cel = GeneratorLiczbLosowych.losowyElement(dostępnePola);
            if (cel != null && pozycja != null) {
                świat.przesuńOrganizm(this, cel);
            }
        }
    }

}
