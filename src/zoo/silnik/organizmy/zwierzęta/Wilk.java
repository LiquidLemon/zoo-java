package zoo.silnik.organizmy.zwierzęta;

import zoo.silnik.Wektor;
import zoo.silnik.organizmy.Zwierzę;
import zoo.silnik.Świat;

public class Wilk extends Zwierzę {
    public Wilk(Świat świat, Wektor pozycja) {
        super(świat, pozycja, 5, 9);
    }

    @Override
    public String pobierzSymbol() {
        return "\uD83D\uDC3A"; // 🐺
    }
}
