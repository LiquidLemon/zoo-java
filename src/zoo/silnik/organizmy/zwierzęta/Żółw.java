package zoo.silnik.organizmy.zwierzęta;

import zoo.narzędzia.GeneratorLiczbLosowych;
import zoo.silnik.Organizm;
import zoo.silnik.Wektor;
import zoo.silnik.organizmy.Zwierzę;
import zoo.silnik.wydarzenia.Unik;
import zoo.silnik.Świat;

public class Żółw extends Zwierzę {
    public Żółw(Świat świat, Wektor pozycja) {
        super(świat, pozycja, 1, 2);
    }

    @Override
    public void akcja() {
        if (GeneratorLiczbLosowych.szansa(0.25)) {
            super.akcja();
        }
    }

    @Override
    public boolean kolizja(Organizm agresor) {
        if (agresor.pobierzSiłę() < 5) {
            emituj(new Unik(this, agresor));
            return true;
        }
        return super.kolizja(agresor);
    }

    @Override
    public String pobierzSymbol() {
        return "\uD83D\uDC22"; // 🐢
    }
}
