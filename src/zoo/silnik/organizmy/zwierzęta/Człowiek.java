package zoo.silnik.organizmy.zwierzęta;

import zoo.narzędzia.GeneratorLiczbLosowych;
import zoo.silnik.Wektor;
import zoo.silnik.organizmy.Zwierzę;
import zoo.silnik.wydarzenia.AktywacjaUmiejętności;
import zoo.silnik.Świat;

public class Człowiek extends Zwierzę {
    private Wektor kierunekRuchu;
    private int czasOdUżyciaUmiejętności;

    public Człowiek(Świat świat, Wektor pozycja) {
        super(świat, pozycja, 4, 5);
        kierunekRuchu = Wektor.ZERO;
        czasOdUżyciaUmiejętności = 10;
    }

    @Override
    public String pobierzSymbol() {
        return umiejętnosćJestAktywna()
            ? "\uD83C\uDFC3" // 🏃
            :"\uD83D\uDEB6️"; // 🚶‍
    }

    @Override
    public void akcja() {
        idź(kierunekRuchu);
        kierunekRuchu = Wektor.ZERO;
    }

    @Override
    public void postarz() {
        super.postarz();
        czasOdUżyciaUmiejętności++;
    }

    @Override
    protected int pobierzZasięg() {
        if (czasOdUżyciaUmiejętności < 3) {
            return 2;
        } else if (czasOdUżyciaUmiejętności < 5) {
            return GeneratorLiczbLosowych.szansa(0.5) ? 2 : 1;
        }
        return 1;
    }

    public void ustawKierunekRuchu(Wektor kierunek) {
        kierunekRuchu = kierunek;
    }

    public void aktywujUmiejętność() {
        if (umiejętnosćJestGotowa()) {
            czasOdUżyciaUmiejętności = 0;
            emituj(new AktywacjaUmiejętności());
        }
    }

    public boolean umiejętnosćJestAktywna() {
        return czasOdUżyciaUmiejętności < 5;
    }

    public boolean umiejętnosćJestGotowa() {
        return czasOdUżyciaUmiejętności >= 10;
    }
}
