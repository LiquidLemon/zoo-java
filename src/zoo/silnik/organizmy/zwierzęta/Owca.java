package zoo.silnik.organizmy.zwierzęta;

import zoo.silnik.Wektor;
import zoo.silnik.organizmy.Zwierzę;
import zoo.silnik.Świat;

public class Owca extends Zwierzę {
    public Owca(Świat świat, Wektor pozycja) {
        super(świat, pozycja, 4, 4);
    }

    @Override
    public String pobierzSymbol() {
        return "\uD83D\uDC11"; // 🐑
    }
}
