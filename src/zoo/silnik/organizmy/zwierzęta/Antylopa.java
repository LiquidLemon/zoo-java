package zoo.silnik.organizmy.zwierzęta;

import zoo.narzędzia.GeneratorLiczbLosowych;
import zoo.silnik.Organizm;
import zoo.silnik.Wektor;
import zoo.silnik.organizmy.Zwierzę;
import zoo.silnik.Świat;

import java.util.Set;

public class Antylopa extends Zwierzę {
    public Antylopa(Świat świat, Wektor pozycja) {
        super(świat, pozycja, 4, 4);
    }

    @Override
    public String pobierzSymbol() {
        return "\uD83D\uDC10"; // 🐐
    }

    @Override
    protected int pobierzZasięg() {
        return GeneratorLiczbLosowych.szansa(0.5) ? 2 : 1;
    }

    @Override
    public boolean kolizja(Organizm agresor) {
        if (GeneratorLiczbLosowych.szansa(0.5)) {
            Set<Wektor> sąsiedzi = świat.pobierzPustychSąsiadów(pozycja);
            Wektor cel = GeneratorLiczbLosowych.losowyElement(sąsiedzi);
            if (cel != null) {
                świat.przesuńOrganizm(this, cel);
                return false;
            }
        }
        return super.kolizja(agresor);
    }
}
