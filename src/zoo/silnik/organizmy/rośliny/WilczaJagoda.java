package zoo.silnik.organizmy.rośliny;

import zoo.silnik.Organizm;
import zoo.silnik.Wektor;
import zoo.silnik.organizmy.Roślina;
import zoo.silnik.Świat;

public class WilczaJagoda extends Roślina {
    public WilczaJagoda(Świat świat, Wektor pozycja) {
        super(świat, pozycja, 99);
    }

    @Override
    public String pobierzSymbol() {
        return "\uD83C\uDF52"; // 🍒
    }

    @Override
    public boolean kolizja(Organizm agresor) {
        boolean rezultat = super.kolizja(agresor);
        if (agresor.jestŻywy()) {
            agresor.zabij();
        }
        return rezultat;
    }
}
