package zoo.silnik.organizmy.rośliny;

import zoo.silnik.Wektor;
import zoo.silnik.organizmy.Roślina;
import zoo.silnik.Świat;

public class Mlecz extends Roślina {
    public Mlecz(Świat świat, Wektor pozycja) {
        super(świat, pozycja, 0);
    }

    @Override
    public void akcja() {
        for (int i = 0; i < 3; i++) {
            super.akcja();
        }
    }

    @Override
    public String pobierzSymbol() {
        return "\uD83C\uDF3B"; // 🌻
    }
}
