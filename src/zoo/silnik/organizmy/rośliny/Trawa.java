package zoo.silnik.organizmy.rośliny;

import zoo.silnik.Wektor;
import zoo.silnik.organizmy.Roślina;
import zoo.silnik.Świat;

public class Trawa extends Roślina {
    public Trawa(Świat świat, Wektor pozycja) {
        super(świat, pozycja, 0);
    }

    @Override
    public String pobierzSymbol() {
        return "\uD83C\uDF31"; // 🌱
    }
}
