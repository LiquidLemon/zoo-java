package zoo.silnik.organizmy.rośliny;

import zoo.silnik.Organizm;
import zoo.silnik.Wektor;
import zoo.silnik.organizmy.Roślina;
import zoo.silnik.Świat;

public class Guarana extends Roślina {
    public Guarana(Świat świat, Wektor pozycja) {
        super(świat, pozycja, 0);
    }

    @Override
    public String pobierzSymbol() {
        return "\uD83D\uDC40"; // 👀
    }

    @Override
    public boolean kolizja(Organizm agresor) {
        if (!super.kolizja(agresor)) {
            agresor.ustawSiłę(agresor.pobierzSiłę() + 3) ;
            return false;
        }
        return true;
    }
}
