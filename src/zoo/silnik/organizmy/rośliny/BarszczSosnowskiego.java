package zoo.silnik.organizmy.rośliny;

import zoo.silnik.Organizm;
import zoo.silnik.Wektor;
import zoo.silnik.organizmy.Roślina;
import zoo.silnik.organizmy.Zwierzę;
import zoo.silnik.wydarzenia.Zatrucie;
import zoo.silnik.Świat;

public class BarszczSosnowskiego extends Roślina {
    public BarszczSosnowskiego(Świat świat, Wektor pozycja) {
        super(świat, pozycja, 10);
    }

    @Override
    public String pobierzSymbol() {
        return "\uD83C\uDF3F"; // 🌿
    }

    @Override
    public void akcja() {
        super.akcja();
        świat.pobierzSąsiadów(pozycja).stream()
                .map((Wektor pozycja) -> świat.pobierzOrganizm(pozycja))
                .filter((Organizm organizm) -> organizm instanceof Zwierzę)
                .forEach(organizm -> {
                    emituj(new Zatrucie(this, organizm));
                    organizm.zabij();
                });
    }

}
