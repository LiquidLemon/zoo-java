package zoo.silnik.organizmy;

import zoo.silnik.Organizm;
import zoo.silnik.Wektor;
import zoo.silnik.Świat;

public abstract class Zwierzę extends Organizm {
    public Zwierzę(Świat świat, Wektor pozycja, int inicjatywa, int siła) {
        super(świat, pozycja, inicjatywa, siła);
    }

    @Override
    public void akcja() {
        Wektor kierunek =
                świat.pobierzLosowegoSąsiada(pozycja).odejmij(pozycja);
        idź(kierunek);
    }

    protected void idź(Wektor kierunek) {
        Wektor cel = pozycja;
        for (int i = 0; i < pobierzZasięg(); i++) {
            if (świat.poleIstnieje(cel.dodaj(kierunek))) {
                cel = cel.dodaj(kierunek);
            } else {
                break;
            }
        }
        if (cel != null && pozycja != null) {
            świat.przesuńOrganizm(this, cel);
        }
    }

    protected int pobierzZasięg() {
        return 1;
    }
}
