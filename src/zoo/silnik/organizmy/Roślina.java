package zoo.silnik.organizmy;

import zoo.narzędzia.GeneratorLiczbLosowych;
import zoo.silnik.Organizm;
import zoo.silnik.Wektor;
import zoo.silnik.Świat;

import java.util.Set;

public abstract class Roślina extends Organizm {
    public Roślina(Świat świat, Wektor pozycja, int siła) {
        super(świat, pozycja, 0, siła);
    }

    @Override
    public void akcja() {
        if (GeneratorLiczbLosowych.szansa(0.2)) {
            rozmnóżSię();
        }
    }

    protected void rozmnóżSię() {
        Set<Wektor> sąsiedzi = świat.pobierzPustychSąsiadów(pozycja);
        Wektor cel = GeneratorLiczbLosowych.losowyElement(sąsiedzi);
        if (cel != null) {
            try {
                Organizm potomek = getClass()
                        .getConstructor(Świat.class, Wektor.class)
                        .newInstance(świat, cel);

                świat.dodajOrganizm(potomek);
            } catch (ReflectiveOperationException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
