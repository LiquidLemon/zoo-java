package zoo.silnik;

import java.io.Serializable;
import java.util.Objects;

public class Wektor implements Serializable {
    public final int x, y;

    public Wektor(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Wektor dodaj(Wektor inny) {
        return new Wektor(x + inny.x, y + inny.y);
    }

    public Wektor odejmij(Wektor inny) {
        return new Wektor(x - inny.x, y - inny.y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Wektor wektor = (Wektor) o;
        return x == wektor.x && y == wektor.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    public static final Wektor ZERO = new Wektor(0, 0);
}
