package zoo.silnik;

import zoo.interfejs.Panel;

import java.util.HashSet;
import java.util.Set;

public class ŚwiatZKwadratów extends Świat {
    public ŚwiatZKwadratów(int wysokość, int szerokość) {
        super(wysokość, szerokość);
    }

    @Override
    public void rysowanie() {
        for (int x = 0; x < szerokość; x++) {
            for (int y = 0; y < wysokość; y++) {
                Organizm organizm = this.plansza[x][y];
                String symbol = organizm == null ? "" : organizm.pobierzSymbol();
                panel.ustaw(new Wektor(x, y), symbol);
            }
        }
    }

    @Override
    public Set<Wektor> pobierzSąsiadów(Wektor pozycja) {
        Set<Wektor> sąsiedzi = new HashSet<>();

        if (pozycja.x > 0) {
            sąsiedzi.add(pozycja.dodaj(new Wektor(-1, 0)));
        }

        if (pozycja.x < szerokość - 1) {
            sąsiedzi.add(pozycja.dodaj(new Wektor(1, 0)));
        }

        if (pozycja.y > 0) {
            sąsiedzi.add(pozycja.dodaj(new Wektor(0, -1)));
        }

        if (pozycja.y < wysokość - 1) {
            sąsiedzi.add(pozycja.dodaj(new Wektor(0, 1)));
        }

        return sąsiedzi;
    }
}
