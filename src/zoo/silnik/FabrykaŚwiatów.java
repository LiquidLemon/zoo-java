package zoo.silnik;

import zoo.narzędzia.GeneratorLiczbLosowych;
import zoo.silnik.organizmy.zwierzęta.Człowiek;

public class FabrykaŚwiatów {
    public static Świat nowyŚwiatZKwadratów(int szerokość, int wysokość) {
        Świat świat = new ŚwiatZKwadratów(wysokość, szerokość);

        for (String gatunek : FabrykaOrganizmów.gatunki()) {
            for (int i = 0; i < 3; i++) {
                Wektor pozycja;
                do {
                    pozycja = new Wektor(
                            GeneratorLiczbLosowych.liczbaCałkowita(szerokość),
                            GeneratorLiczbLosowych.liczbaCałkowita(wysokość)
                    );
                } while (świat.pobierzOrganizm(pozycja) != null);
                Organizm organizm =
                        FabrykaOrganizmów.nowyOrganizm(gatunek, świat, pozycja);
            }
        }

        return świat;
    }
}
