package zoo.interfejs;

import zoo.silnik.FabrykaOrganizmów;
import zoo.silnik.Wektor;
import zoo.silnik.Świat;

import javax.swing.*;

public class KreatorOrganizmów extends JPopupMenu {
    KreatorOrganizmów(Świat świat, Wektor pozycja) {
        super();
        for (String gatunek : FabrykaOrganizmów.gatunki()) {
            JMenuItem element = new JMenuItem(gatunek);
            element.addActionListener((event) -> {
                FabrykaOrganizmów.nowyOrganizm(gatunek, świat, pozycja);
                świat.rysowanie();
            });
            add(element);
        }
    }
}
