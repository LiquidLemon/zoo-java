package zoo.interfejs;

import zoo.silnik.Wektor;
import zoo.silnik.Świat;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PanelZKwadratów extends Panel {
    static final int wymiary = 20;

    private JButton[][] przyciski;
    Świat świat;

    public PanelZKwadratów(Świat świat) {
        this.świat = świat;
        int wysokość = świat.pobierzWysokość();
        int szerokość = świat.pobierzSzerokość();


        setLayout(new GridLayout(wysokość, szerokość));
        setSize(wymiary * szerokość, wymiary * wysokość);
        przyciski = new JButton[szerokość][wysokość];

        for (int y = 0; y < wysokość; y++) {
            for (int x = 0; x < szerokość; x++) {
                JButton przycisk = new JButton();
                przycisk.setPreferredSize(new Dimension(wymiary, wymiary));
                przycisk.setMargin(new Insets(0, 0, 0, 0));
                przycisk.setBounds(x * wymiary, y * wymiary, wymiary, wymiary);
                przycisk.setFocusable(false);

                Wektor pozycja = new Wektor(x, y);
                JPopupMenu kreator = new KreatorOrganizmów(świat, pozycja);
                JPopupMenu menuInformacyjne = new JPopupMenu();
                JLabel etykieta = new JLabel();
                menuInformacyjne.add(etykieta);
                przycisk.addActionListener((ActionEvent e) -> {
                    System.out.println(pozycja);
                });
                przycisk.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent event) {
                        if (event.isPopupTrigger()) {
                            if (świat.pobierzOrganizm(pozycja) == null) {
                                kreator.show(
                                        event.getComponent(),
                                        event.getX(), event.getY()
                                );
                            } else {
                                etykieta.setText(świat.pobierzOrganizm(pozycja)
                                        .getClass().getSimpleName());

                                menuInformacyjne.show(
                                        event.getComponent(),
                                        event.getX(), event.getY()
                                );
                            }
                        }
                    }
                });
                add(przycisk);
                przyciski[x][y] = przycisk;
            }
        }
    }

    @Override
    public void ustaw(Wektor pozycja, String treść) {
        przyciski[pozycja.x][pozycja.y].setText(treść);
    }
}
