package zoo.interfejs;

import zoo.silnik.Wektor;

import javax.swing.*;

public abstract class Panel extends JPanel {
    public abstract void ustaw(Wektor pozycja, String treść);
}
