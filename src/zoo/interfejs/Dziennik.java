package zoo.interfejs;

import javax.swing.*;

public class Dziennik extends JTextArea {
    public Dziennik() {
        setColumns(30);
        setRows(30);
        setEditable(false);
    }

    public void wyczyść() {
        setText("");
    }

    public void dodajWiadomość(Object wiadomość) {
        append(wiadomość.toString() + "\n");
    }
}
