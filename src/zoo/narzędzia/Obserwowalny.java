package zoo.narzędzia;

import java.util.ArrayList;
import java.util.List;

public abstract class Obserwowalny<T> {
    List<Obserwator<T>> obserwatorzy;

    public Obserwowalny() {
        obserwatorzy = new ArrayList<>();
    }

    public void dodajObserwatora(Obserwator<T> obserwator) {
        obserwatorzy.add(obserwator);
    }

    protected void emituj(T wiadomość) {
        for (Obserwator<T> obserwator : obserwatorzy) {
            obserwator.odbierz(wiadomość);
        }
    }
}
