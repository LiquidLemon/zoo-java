package zoo.narzędzia;

import java.util.Collection;
import java.util.Random;

public class GeneratorLiczbLosowych extends Random {
    static private GeneratorLiczbLosowych instancja =
            new GeneratorLiczbLosowych();


    private GeneratorLiczbLosowych() {
        super();
    }

    static public int liczbaCałkowita(int górnyLimit) {
        return instancja.nextInt(górnyLimit);
    }

    public static <T> T losowyElement(Collection<T> kolekcja) {
        if (kolekcja.isEmpty()) {
            return null;
        }

        int indeks = liczbaCałkowita(kolekcja.size());
        int i = 0;
        for (T element : kolekcja) {
           if (i == indeks) {
               return element;
           }
           i++;
        }

        throw new RuntimeException("Indeks poza kolekcją");
    }

    public static boolean szansa(double szansa) {
        return szansa >= instancja.nextDouble();
    }
}
