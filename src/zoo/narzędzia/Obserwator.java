package zoo.narzędzia;

@FunctionalInterface
public interface Obserwator<T> {
    void odbierz(T wiadomość);
}
